const axios = require('axios')
let Parser = require('rss-parser');
let parser = new Parser();

module.exports = {

    getJSONdata: async() => {
        let getCurrentPostNumber = await axios.get('https://xkcd.com/info.0.json');
        let getJSONdataArr = []
        if (getCurrentPostNumber && getCurrentPostNumber.data && getCurrentPostNumber.data.num) {
            getCurrentPostNumber.data.num

            for (let i = getCurrentPostNumber.data.num; i > getCurrentPostNumber.data.num - 10; i--) {


                let getPost = await axios.get(`https://xkcd.com/${i}/info.0.json`);
                if (getPost && getPost.data) {
                    if (getPost.data.day && getPost.data.month && getPost.data.year) {
                        let str = `${getPost.data.year}-${getPost.data.month}-${getPost.data.day}`
                        getPost.data.published = new Date(str).toISOString();
                        if (!getPost.data.description) getPost.data.description = getPost.data.title
                        if (!getPost.data.alt) getPost.data.alt = getPost.data.title
                        getPost.data.link = `https://xkcd.com/${i}/`
                    }
                    getJSONdataArr.push(getPost.data)
                }
            }
        }
        return getJSONdataArr
    },
    getXMLdata: async() => {
        let feed = await parser.parseURL('http://feeds.feedburner.com/PoorlyDrawnLines');
        let getXMLdataArr = []
        feed.items.forEach(item => {
            let sources = item['content:encoded'].match(/<img [^>]*src="[^"]*"[^>]*>/gm).map(x => x.replace(/.*src="([^"]*)".*/, '$1'));
            let obj = {
                title: item.title,
                published: item.pubDate,
                link: item.link,
                img: sources[0],
                description: item.title,
                alt: item.title
            }
            getXMLdataArr.push(obj)
        });
        return getXMLdataArr
    }
}
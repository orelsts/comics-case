const express = require('express')
const app = express()

const port = 8080
const expbs = require('express-handlebars');
const path = require('path');
let service = require('./service')


app.set('views', path.join(__dirname, '/'));
const hbs = expbs.create({
    extname: '.hbs',
    defaultLayout: false,
    layoutsDir: path.join(__dirname, '/'),
    helpers: require('./utils/handebar-helpers'),
});
app.engine('.hbs', hbs.engine);
app.set('view engine', '.hbs');
app.use(express.static(__dirname + '/public'));


app.get('/', async(req, res) => {
    try {
        let resultArr = []

        let getJSONdata = await service.getJSONdata()
        resultArr.push(getJSONdata)

        let getXMLdata = await service.getXMLdata()
        resultArr.push(getXMLdata)

        resultArr = resultArr.flat()
        resultArr.map((el, i) => el.indexNum = i + 1)
        resultArr.sort((a, b) => a.published > b.published)
    
        res.render('index', {
            blocks: resultArr
        });
    } catch (error) {
        throw error
    }
    
    


   
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
const service = require('./service')

test('getJSONdataCheckDateLength', async() => {
    let result = await service.getJSONdata()
    expect(result.length).toBe(10);
});

test('getJSONdataCheckDateIfKeysExist', async() => {
    let result = await service.getJSONdata()
    for (let item of result) {
        expect(item.title).toBeDefined()
        expect(item.published).toBeDefined()
        expect(item.link).toBeDefined()
        expect(item.img).toBeDefined()
        expect(item.description).toBeDefined()
        expect(item.alt).toBeDefined()
    }

});

test('getXMLdataCheckDateLength', async() => {
    let result = await service.getXMLdata()
    expect(result.length).toBe(10);
});

test('getXMLdataCheckDateIfKeysExist', async() => {
    let result = await service.getXMLdata()
    for (let item of result) {
        expect(item.title).toBeDefined()
        expect(item.published).toBeDefined()
        expect(item.link).toBeDefined()
        expect(item.img).toBeDefined()
        expect(item.description).toBeDefined()
        expect(item.alt).toBeDefined()
    }

});
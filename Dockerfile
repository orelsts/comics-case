FROM node:14.16.0


WORKDIR /

COPY . /
RUN npm install
RUN npm update
EXPOSE 8080



CMD node server.js

